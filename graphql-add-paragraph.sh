#!/bin/bash

# Funzione per eseguire la query GraphQL
function execute_graphql_query {
    local query=$1
    echo "Mutation:"
    echo "$query"
}

# File JSON di esempio
JSON_FILE="./paragraphe-10.json"

# Leggi il contenuto del file JSON
data=$(cat "$JSON_FILE")

# Estrai le informazioni necessarie per la mutation
chapitre=$(echo "$data" | jq -r '.chapitre')
titre=$(echo "$data" | jq -r '.titre')
technique=$(echo "$data" | jq -r '.technique')
theorie=$(echo "$data" | jq -r '.theorie')
citations=$(echo "$data" | jq -r '.citations | join(", ")')
commentaire=$(echo "$data" | jq -r '.commentaire')
texte=$(echo "$data" | jq -r '.texte | map(.id + ": " + .contenu) | join("\n")')

# Costruisci la stringa per la mutation GraphQL
query='mutation {
  addParagraph(
    chapitre: "'"$chapitre"'",
    titre: "'"$titre"'",
    technique: "'"$technique"'",
    theorie: "'"$theorie"'",
    citations: "'"$citations"'",
    commentaire: "'"$commentaire"'",
    texte: """'"$texte"'"""
  ) {
    id
    chapitre
    titre
    technique
    theorie
    citations
    commentaire
    texte {
      id
      contenu
    }
  }
}'

# Esegui la query GraphQL
execute_graphql_query "$query"

