require('dotenv').config()

const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
const swaggerJsdoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')

// Connexion à la base de données
mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true })
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('Connected to Database'))

app.use(cors()); // Enable CORS

app.use(express.json())

// Configuration Swagger
const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'rest',
      version: '1.0.0',
      description: 'description',
    },
  },
  apis: ['./routes/*.js'], // Percorso ai file di route
};

const specs = swaggerJsdoc(options);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));

// Provisoire : define a route for the root URL
//app.get('/', (req, res) => {
//    res.send('Welcome to the API')
//  })

const paragraphesRouter = require('./routes/paragraphes')
app.use('/paragraphes', paragraphesRouter)

const chapitresRouter = require('./routes/chapitres')
app.use('/chapitres', chapitresRouter)

app.listen(3000, () => console.log('Server Started'))