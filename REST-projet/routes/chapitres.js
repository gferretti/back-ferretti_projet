const express = require('express');
const router = express.Router();
const {Paragraph, Chapter} = require('../models/texte');

// Get tous les chapitres
router.get('/', async (req, res) => {
  try {
    const chapters = await Chapter.find();
    res.json(chapters);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Get tous les titres des chapitres
router.get('/titre', async (req, res) => {
  try {
    const chapters = await Chapter.find({}, 'titre');
    res.json(chapters);
  } catch (error) {
    res.status(500).json({ error: 'Server error' });
  }
});

// Get un chapitre
router.get('/:id', getChapter, (req, res) => {
  res.json(res.chapter);
});

// Post un chapitre
router.post('/', async (req, res) => {
  const chapter = new Chapter({
    titre: req.body.titre,
    numeroChapitre: req.body.numeroChapitre,
    resume: req.body.resume,
    paragraphes: req.body.paragraphes,
    tag: req.body.tag
  });
  try {
    const newChapter = await chapter.save();
    res.status(201).json(newChapter);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Updating a chapter
router.patch('/:id', getChapter, async (req, res) => {
  if (req.body.categorie != null) {
    res.chapter.categorie = req.body.categorie;
  }
  if (req.body.paragraphes != null) {
    res.chapter.paragraphes = req.body.paragraphes;
  }
  try {
    const updatedChapter = await res.chapter.save();
    res.json(updatedChapter);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

/* Deleting a chapter
router.delete('/:id', getChapter, async (req, res) => {
  try {
    await res.chapter.remove();
    res.json({ message: 'Deleted Chapter' });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});
*/

//prova2 - Deleting a chapter
router.delete('/:id', getChapter, async (req, res) => {
  try {
    await Chapter.deleteOne({ _id: res.chapter._id });
    res.json({ message: 'Deleted Chapter' });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

async function getChapter(req, res, next) {
  let chapter;
  try {
    chapter = await Chapter.findById(req.params.id);
    if (chapter == null) {
      return res.status(404).json({ message: 'Cannot find chapter' });
    }
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }

  res.chapter = chapter;
  next();
}

module.exports = router;

/* test 2 -- getting sousCategorie
router.get('/sousCategorie', async (req, res) => {
  try {
    const sousCategories = await Chapter.distinct('sousCategorie');
    res.json(sousCategories);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
});
*/

/* Getting sousCategorie
router.get('/paragraphes', async (req, res) => {
  const subcategory = req.query.sousCategorie;
  try {
    let paragraphs;
    if (subcategory) {
      paragraphs = await Chapter.aggregate([
        { $unwind: "$paragraphes" },
        { $match: { "paragraphes.sousCategorie": subcategory } },
        { $project: { _id: 0, "paragraphes": 1 } }
      ]);
    } else {
      paragraphs = await Chapter.find({}, { paragraphes: 1 });
    }
    const allParagraphs = paragraphs.flatMap((chapter) => chapter.paragraphes);
    res.json(allParagraphs);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});
*/