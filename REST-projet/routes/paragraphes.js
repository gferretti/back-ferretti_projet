const express = require('express')
const router = express.Router()
const {Paragraph, Chapter} = require('../models/texte')

// Filtre par "technique" et "théorie"
router.get('/', async (req, res) => {
  const technique = req.query.technique;
  const theorie = req.query.theorie;
  try {
    let paragraphs;
    if (theorie && technique) {
      paragraphs = await Paragraph.find({ "theorie": theorie, "technique": technique });
    } else if (technique) {
      paragraphs = await Paragraph.find({ technique: technique });
    } else if (theorie) {
      paragraphs = await Paragraph.find({ theorie: theorie });
    } else {
      paragraphs = await Paragraph.find();
    }
    res.json(paragraphs);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

/**
 * @swagger
 * /api/paragraphes:
 *   get:
 *     summary: Ottiene tutti i paragrafi
 *     responses:
 *       200:
 *         description: Successo. Restituisce un array di paragrafi.
 *       500:
 *         description: Errore del server.
 */
// Get tous les paragraphes
router.get('/', async (req, res) => {
  try {
    const paragraphs = await Paragraph.find();
    res.json(paragraphs);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Get tous les contenus
router.get('/texte/contenu', async (req, res) => {
  try {
    const paragraphs = await Paragraph.find({}, 'texte.contenu');
    res.json(paragraphs);
  } catch (error) {
    res.status(500).json({ error: 'Server error' });
  }
});

// Getting un paragraphe
router.get('/:id', getParagraph, (req, res) => {
  res.json(res.paragraph);
});

// Post un paragraphe
router.post('/', async (req, res) => {
  const paragraph = new Paragraph({
    chapitre: req.body.chapitre,
    titre: req.body.titre,
    theorie: req.body.theorie,
    technique: req.body.technique,
    citations: req.body.citations,
    commentaire: req.body.commentaire,
    texte: req.body.texte,
  });
  try {
    const newParagraph = await paragraph.save();
    res.status(201).json(newParagraph);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Updating One
router.patch('/:id', getParagraph, async (req, res) => {
  if (req.body.chapitre != null) {
    res.paragraph.chapitre = req.body.chapitre;
  }
  if (req.body.titre != null) {
    res.paragraph.titre = req.body.titre;
  }
  if (req.body.theorie != null) {
    res.paragraph.theorie = req.body.theorie;
  }
  if (req.body.citations != null) {
    res.paragraph.citations = req.body.citations;
  }
  if (req.body.commentaire != null) {
    res.paragraph.commentaire = req.body.commentaire;
  }
  if (req.body.texte != null) {
    res.paragraph.texte = req.body.texte;
  }
  try {
    const updatedParagraph = await res.paragraph.save();
    res.json(updatedParagraph);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Update the comment of a paragraph
router.patch('/:id/commentaire', async (req, res) => {
  try {
    const paragraph = await Paragraph.findByIdAndUpdate(
      req.params.id,
      { commentaire: req.body.commentaire },
      { new: true, runValidators: true }
    );

    if (!paragraph) {
      return res.status(404).json({ message: 'Paragraph not found' });
    }

    res.json(paragraph);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Deleting One
router.delete('/:id', getParagraph, async (req, res) => {
  try {
    await Paragraph.deleteOne({ _id: res.paragraph._id });
    res.json({ message: 'Deleted Paragraph' });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

module.exports = router;

async function getParagraph(req, res, next) {
  let paragraph
  try {
    paragraph = await Paragraph.findById(req.params.id)
    if (paragraph == null) {
      return res.status(404).json({ message: 'Cannot find paragraph' })
    }
  } catch (err) {
    return res.status(500).json({ message: err.message })
  }

  res.paragraph = paragraph;
  next()
}

module.exports = router

// Filter by theorie
/*
router.get('/', async (req, res) => {
  const theorie = req.query.theorie;
  try {
    let paragraphs;
    if (theorie) {
      paragraphs = await Paragraph.find({ theorie: theorie });
    } else {
      paragraphs = await Paragraph.find();
    }
    res.json(paragraphs);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});
*/