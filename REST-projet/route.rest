
GET http://localhost:3000/chapitres

###

GET http://localhost:3000/chapitres/titre

###

GET http://localhost:3000/paragraphes?theorie=critical-code-studies

###

GET http://localhost:3000/paragraphes?theorie=protocoles&technique=TCP-IP

###

GET http://localhost:3000/paragraphes?theorie=nouveau-materialisme

###

GET http://localhost:3000/paragraphes/texte/contenu


###

POST http://localhost:3000/chapitres
Content-Type: application/json

{
    "titre": "Contexte et méthodologie",
    "numeroChapitre": 1,
    "resume": "Le chapitre présente le sujet du projet de thèse, à savoir l'impact des technologies numériques sur notre monde, en se concentrant en particulier sur le rôle des protocoles. Les champs disciplinaires du nouveau matérialisme et des études de codes critiques sont évoqués pour décrire le cadre théorique de ce travail.",
    "paragraphes": [
      {
        "numeroParagraphe": 1,
        "titreParagraphe": "S'intéresser au numérique pour comprendre le présent"
      },

      {
        "numeroParagraphe": 2,
        "titreParagraphe": "Code : entre l'humain et la machine"
      },

      {
        "numeroParagraphe": 3,
        "titreParagraphe": "Protocoles informatiques"
      }
    ],
    "tag": "Projet de thèse"
  }

###

POST http://localhost:3000/paragraphes/
Content-Type: application/json

{
    "chapitre": "Contexte et méthodologie",
    "titre": "S'intéresser au numérique pour comprendre le présent",
    "theorie": "Critical code studies",
    "citations": ["Cavallari", "Doueihi", "McLuhan"],
    "commentaire": "",
    "texte": [
        {
            "id": "paragraphe1",
            "contenu": "Les technologies numériques structurent l’univers dans lequel nous vivons. Elles déterminent temporellement et spatialement la réalité et influencent les représentations, c’est-à-dire notre manière d’appréhender le monde en tant que totalité composée d’objets stables et clairement définis. Pensons, par exemple, aux appareils mobiles. Ils nous incitent à construire continuellement notre présence en ligne et à diriger notre attention vers la construction d’autres présences, en systématisant une gestuelle « déambulatoire et scripturale » (Cavallari, 2017). Nous marchons effectivement la tête baissée en tambourinant sur l’écran pour répondre à un message ou nous nous déplaçons dans l’espace en suivant la ligne indiquée par notre navigateur de confiance, en passant nos doigts sur la surface du dispositif pour mieux comprendre où aller. Ces gestuelles jouent un rôle central dans la constitution de notre sphère d’action et de perception, déterminant, notamment, la manière dont nous vivons et comprenons la ville (Cavallari, 2017). Des dynamiques similaires peuvent être observées dans de nombreux autres contextes : la production et la circulation de l’information, les transactions économiques et la manière dont nous définissons et incarnons les idées de personne et de mémoire sont désormais façonnées et rendues possibles par des technologies informatiques. La confrontation avec ces dispositifs est donc indispensable à toute réflexion sur le présent."
        },

        {
            "id": "paragraphe2",
            "contenu": "De manière complémentaire, tenter de comprendre le sens de ces dispositifs nous interpelle sur notre façon de connaître et de vivre dans le monde. La notion de « culture numérique » élaborée par Milad Doueihi théorise précisément cette influence mutuelle, en mettant l’accent sur la portée des dispositifs numériques dans la production et la circulation des connaissances. Il s’agit d’une transformation radicale qui concerne « la nature même des objets de notre savoir » et « l’espace censé les accueillir et les faire circuler » (2011, p. 11)."
        },

        {
            "id": "paragraphe3",
            "contenu": "Pour clarifier cette notion, Doueihi fait référence au concept d’amitié, montrant que la culture numérique reprend et, en même temps, remodèle profondément la définition classique de cette notion. Il se réfère plus particulièrement aux cas de Facebook et de Wikipédia. D’une part, la définition aristotélicienne d’amitié présuppose une condition d’égalité et d’équivalence entre les sujets, excluant ainsi tout ordre hiérarchique ou relation de pouvoir. D’autre part, alors que Facebook présente une radicalisation du concept, établissant une communauté qui oublie les spécificités et les besoins individuels, Wikipédia exclut toute relation personnelle entre les utilisateurs, identifiant la notion d’égalité avec l’anonymat des auteurs. Tandis que dans la modélisation de Facebook, le partage est poussé à l’extrême et concerne tous les domaines, Wikipédia initie une interaction exclusivement tournée vers le savoir, lui-même identifié à l’information objective."
        },

        {
            "id": "paragraphe4",
            "contenu": "La culture numérique n’existe pas indépendamment de la culture tout court, la modélisation qu’elle propose en dépend directement. L’exemple sur la notion d’amitié proposé par Doueihi nous montre que les outils numériques sont capables de modifier notre monde parce qu’ils en font essentiellement partie."
        },

        {
            "id": "paragraphe5",
            "contenu": "En d’autres termes, l’impact transformateur du dispositif n’est pas une invention créée ex novo, mais est également déterminé par les cultures, les pratiques, les institutions, la disponibilité de matériel, les dynamiques spatiales et temporelles au sein desquelles le dispositif lui-même est constitué et exerce son influence. La célèbre expression de Marshall McLuhan – « medium is the message » – (Mcluhan et Lapham, 1994) pourrait ainsi être intégrée à partir de ces réflexions. Le message dépend de la manière dont le média prend en compte et formate le réel, qui n’est jamais neutre. Le réel peut être transformé par le medium, même radicalement, mais ne peut à son tour manquer d’avoir un impact sur la constitution du support lui-même."
        }



    ]
  }




###

DELETE http://localhost:3000/chapitres/648f35920e84fe7289307b77

###

PATCH http://localhost:3000/paragraphes/648e15b1290c5c8c120c957f 
Content-Type: application/json

###

PATCH http://localhost:3000/paragraphes/64909ebbcdacf400f270c7d8
Content-Type: application/json

{
  "commentaire": "changement d'un commentaire"
}

