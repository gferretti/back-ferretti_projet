const mongoose = require('mongoose')

const paragraphSchema = new mongoose.Schema({
  chapitre: {
    type: String,
    required: true
  },
  titre: {
    type: String,
    required: true
  },
  theorie: {
    type: String,
    required: false
  },
  technique: {
    type: String,
    required: false
  },
  citations: {
    type: [String],
    required: false
  },
  commentaire: {
    type: String,
    required: false
  },
  texte: [{
    id: {
      type: String,
      required: true
    },
    contenu: {
      type: String,
      required: true
    }
  }]
});

const chapterSchema = new mongoose.Schema({
  titre: 
    { type: String, 
    required: true },

  numeroChapitre: 
    { type: Number, 
    required: true },

  resume: 
    { type: String, 
      required: true },

  paragraphes: [
    {
      numeroParagraphe: 
      { type: Number, 
        required: true },

      titreParagraphe: 
      { type: String, 
        required: true }
    }
  ],

  tag: 
    { type: String, 
    required: false }
});

module.exports = {
  Paragraph: mongoose.model('Paragraph', paragraphSchema),
  Chapter: mongoose.model('Chapter', chapterSchema),
};
